import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  static const name = 'home-page';

  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: IconButton(
          icon: const Icon(Icons.reorder),
          onPressed: () {},
        ),
        appBar: AppBar(
          title: Row(
            children: [
              Container(
                alignment: Alignment.centerLeft,
                child: IconButton(
                  icon: const Icon(Icons.reorder),
                  onPressed: () {},
                ),
              ),
              const Center(
                child: Text(
                  'Facily Driver',
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
          actions: [
            IconButton(
              onPressed: () {},
              icon: const Icon(Icons.cached),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Row(),
        ),
      ),
    );
  }
}
