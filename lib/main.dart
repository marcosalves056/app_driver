import 'package:app_driver/ui/pages/home_page.dart';
import 'package:flutter/material.dart';

import 'locator.dart';

void main() {
  setUpLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(primarySwatch: Colors.blue),
      initialRoute: HomePage.name,
      routes: {
        HomePage.name: (_) => const HomePage(),
      },
    );
  }
}
