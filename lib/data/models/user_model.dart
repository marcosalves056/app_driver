class UserModel {
  int idDriver;
  String firstName;
  String lastName;
  UserModel(
      {required this.idDriver,
      required this.firstName,
      required this.lastName});

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
        idDriver: json['ID'],
        firstName: json['data']['first_name'],
        lastName: json['data']['last_name']);
  }
}
