import 'dart:html';

class LoadDriverModel {
  int id;
  bool accepted;
  DateTime? acceptedDate;
  DateTime? loadedDate;

  LoadDriverModel(
      {required this.id,
      required this.accepted,
      this.acceptedDate,
      this.loadedDate});

  factory LoadDriverModel.fromJson(Map<String, dynamic> json) {
    return LoadDriverModel(
      id: json['items']['id'],
      accepted: json['items']['accepted'],
      acceptedDate: json['items']['accepted_at'],
      loadedDate: json['items']['loaded_at'],
    );
  }
}
