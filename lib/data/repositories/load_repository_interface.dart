import 'package:app_driver/data/models/load_model.dart';

abstract class LoadDriverRepositoryInterface {
  Future<LoadDriverModel> getLoad(int idDriver, Map<String, dynamic>? params);
}
