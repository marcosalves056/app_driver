import 'package:app_driver/data/models/user_model.dart';

abstract class UserRepositoryInterface {
  Future<UserModel> getUser();

  Future<bool> isPendency(int idDriver);

  Future<bool> loadDriver(int idDriver);
}
