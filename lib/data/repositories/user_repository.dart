import 'package:app_driver/data/models/user_model.dart';
import 'package:app_driver/data/repositories/user_repository_interface.dart';
import 'package:app_driver/data/services/http_service.dart';
import 'package:app_driver/shared/strings.dart';

class UserRepository implements UserRepositoryInterface {
  final HttpService _service;
  final String _baseUrl = Strings.apiUser;
  final _token = Strings.token;
  UserModel? user;

  UserRepository(this._service);

  @override
  Future<UserModel> getUser() async {
    final uri = '$_baseUrl/users-me/';
    final headers = {'Autorization': _token};
    final response = await _service.getRequest(uri, headers: headers);

    user = UserModel.fromJson(response.content!);
    print(user!.idDriver);
    return user!;
  }

  @override
  Future<bool> isPendency(idDriver) async {
    final uri = '$_baseUrl/driver-pendency?driver_id=$idDriver';
    final headers = {'Autorization': _token};
    final response = await _service.getRequest(uri, headers: headers);

    if (response.content!['orders'][0] != null) {
      return true;
    }
    return false;
  }
}
