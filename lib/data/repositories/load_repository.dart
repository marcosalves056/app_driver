import 'package:app_driver/data/models/load_model.dart';
import 'package:app_driver/data/repositories/load_repository_interface.dart';
import 'package:app_driver/data/services/http_service.dart';
import 'package:app_driver/shared/strings.dart';

class LoadDriverRepository implements LoadDriverRepositoryInterface {
  final HttpService _service;
  final String _baseUrl = Strings.apiUser;
  final _token = Strings.token;
  LoadDriverModel? list;

  LoadDriverRepository(this._service);
  @override
  Future<LoadDriverModel> getLoad(
      int idDriver, Map<String, dynamic>? params) async {
    final uri = '$_baseUrl/load-truck?driver_id=$idDriver';
    final headers = {'Autorization': _token};
    final response = await _service.getRequest(uri, headers: headers);

    return LoadDriverModel.fromJson(response.content!);
  }
}
