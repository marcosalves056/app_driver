import 'package:get_it/get_it.dart';

import 'data/services/http_service.dart';

GetIt getIt = GetIt.instance;

void setUpLocator() {
  getIt.registerLazySingleton(() => HttpService());
}
